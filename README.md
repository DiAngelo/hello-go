# hello-g

> Simple stupid project to play with golang's template language

## Purpose

helm, the kubernetes deployment tool, includes a bunch of functions to help create manifests.  It includes [sprig](https://github.com/Masterminds/sprig) template functions.   This project allows to play around with the template format easily without helm.  It's much easier to run this program get the template correct and copy to the appropriate manifest, rather than using helm template, or helm install --dry-run

This project was helpful to create a template that can conditionally populate an image tag whether a variable is populated:


## Running

To run, execute `go run .`  That command will execute the `main()` func in hello.go.  That function invokes `runTemplateExample(), found in hello-template.go.

## Files



