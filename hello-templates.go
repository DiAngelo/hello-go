package main

import "fmt"
import "os"

import "github.com/Masterminds/sprig/v3"
import "text/template"

func runTemplateExample () {
   /* 
      
     Playing with templates
    
  
    */
  
   // setup variableMap to be passed in to template
   variableMap := map[string]interface {} {
     "Name": "Brian DiAngelo",
     "Path": "foo/bar/car/bar/",
     "globalBaseRepo": "docker.foo/app",
     "image": "busybox:latest",
     "env": map[string]interface {} {
       "whoamix": "rbdiang",
     },
   }
  
  
   // create actual template using multi line String
   tpl := `
  
     Hello, {{ lower .Name }}
     
     Strip last element  path from {{.Path -}} 
     
     = {{- dir .Path }}


    image: {{ with .env.whoami }} 
             {{- printf "%s/%s" ( dir $.globalBaseRepo )  .  -}}
           {{ else }} 
             {{- .globalBaseRepo -}} 
           {{ end -}}
           / {{- .image }}
  
   `
  
   // add text functions from sprig to template engine
   fmap := sprig.TxtFuncMap()
   templateEngine := template.Must(template.New("test").Funcs(fmap).Parse(tpl))

   // execute the templateEngine and write output to standard out
   err := templateEngine.Execute(os.Stdout, variableMap)
  
   // print the error if it occurs
   if err != nil {
      fmt.Printf("Error during template execution: %s", err)
      return
   }
}
